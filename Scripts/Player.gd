extends RigidBody3D

## How much vertical force to apply when moving.
@export_range(750.0, 3000.0) var thrust: float = 1000.0

## How much rotation force to apply when rotating.
@export_range(75.0, 300.0) var torque_thrust: float = 100.0

@onready var explosion_audio: AudioStreamPlayer = $ExplosionAudio
@onready var success_audio: AudioStreamPlayer = $SuccessAudio
@onready var rocket_audio: AudioStreamPlayer3D = $RocketAudio
@onready var booster_particles: GPUParticles3D = $BoosterParticles
@onready var right_booster_particles: GPUParticles3D = $RightBoosterParticles
@onready var left_booster_particles: GPUParticles3D = $LeftBoosterParticles
@onready var explosion_particles: GPUParticles3D = $ExplosionParticles
@onready var success_particles: GPUParticles3D = $SuccessParticles


var is_transitioning: bool = false

func _ready():
	booster_particles.emitting = false
	right_booster_particles.emitting = false
	left_booster_particles.emitting = false


func _process(delta: float) -> void:
	# Boost
	if Input.is_action_pressed("boost") and not rocket_audio.playing:
		rocket_audio.play()

	if Input.is_action_pressed("boost"):
		apply_central_force(basis.y * delta * thrust)
		booster_particles.emitting = true


	if Input.is_action_just_released("boost"):
		booster_particles.emitting = false
		rocket_audio.stop()
	
	# Rotate Left
	if Input.is_action_pressed("rotate_left"):
		apply_torque(Vector3(0.0, 0.0, torque_thrust * delta))
		right_booster_particles.emitting = true
	
	if Input.is_action_just_released("rotate_left"):
		right_booster_particles.emitting = false
	
	# Rote Right
	if Input.is_action_pressed("rotate_right"):
		apply_torque(Vector3(0.0, 0.0, -torque_thrust * delta))
		left_booster_particles.emitting = true
	
	if Input.is_action_just_released("rotate_right"):
		left_booster_particles.emitting = false
	
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()


func transition() -> void:
	set_process(false)
	is_transitioning = true


func _on_body_entered(body: Node) -> void:
	if is_transitioning:
		return

	if body.is_in_group("Hazard"):
		crash_sequence()

	if body.is_in_group("Goal"):
		complete_level(body.file_path)


func crash_sequence() -> void:
	print("You crashed")
	transition()
	
	explosion_particles.emitting = true
	rocket_audio.stop()
	explosion_audio.play()
	await explosion_audio.finished
	get_tree().reload_current_scene()


func complete_level(next_level: String) -> void:
	print("You win!")
	transition()
	
	success_particles.emitting = true
	rocket_audio.stop()
	success_audio.play()
	await success_audio.finished
	get_tree().change_scene_to_file(next_level)
